$(document).ready(function () {

    // TODO: Your code here

    const d = $('.dolphin');

//when radio button is clicked - change background
    $("#choose-background").find("input").click(function () {


        const buttonID = $(this).attr('id');
        const split = buttonID.split('-');

        //split is an array and has 2 parts, we are getting rid of 0 = radio because we don't need that long as identifier. [1] is the second part of the split. we are adding either jpg or gif to these files so they all work.

        let newSource;
        if (split[1] === "background6") {
            newSource = split[1] + ".gif";
        } else {
            newSource = split[1] + ".jpg";
        }

        $("#background").attr("src", "../images/" + newSource);

    });


//    time to hide some dolphins

    d.hide();

//    now showing the first dolphin by default
    $('#dolphin1').show();


//    start the coding magic


    $('input[type = checkbox]').click(function () {

        d.hide();
        for (let i of $("input[type=\"checkbox\"]")) {
            if ($(i).prop('checked')) {
                const boxId = $(i).attr('id');
                const split = boxId.split('-');
                //split is an array with 2 things, 0 = radio, don't need
                const selectedImgId = split[1];

                //then put back the ones which are selectedImgId
                $('#' + selectedImgId).show();

            }
        }


    });

    /*Create variables which hold the value of the sliders
    * x is horizontal    y is vertical       s is size*/

    var dScale=1,dTransX=0,dTransY=0;

    /* changing the values of the sliders*/



    function setTransformState(){
        $('.dolphin').each(function () {
            $(this).css('transform','translateX('+dTransX+'%) translateY('+dTransY+'%) scale('+dScale+')');
            console.log("hiya")
        });
    }

    $('[id$=-control]').change(function () {
        console.log("slider " + $(this).attr('id') + " changed");
        console.log($(this).val());
        if ($(this).attr('id') == 'horizontal-control') {
            dTransX = $(this).val();
        } else if ($(this).attr('id') == 'vertical-control') {
            dTransY = $(this).val();
        } else if ($(this).attr('id') == 'size-control') {
            dScale = $(this).val();
        }
        ;

        if (dScale < 0) {
            dScale = 1 - Math.abs(dScale) / 100;
        } else if (dScale == 0) {
            dScale = 1;
        } else if (dScale==2){
            dScale=1.05;
        } else if (dScale>1){
            dScale = Math.log(dScale);
        }
        console.log("dtransX: "+ dTransX);
        console.log("dtransY: "+ dTransY);
        console.log("dScale: "+ dScale);
        setTransformState();

    })

});
